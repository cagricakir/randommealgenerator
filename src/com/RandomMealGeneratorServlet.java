package com;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.JsonResponse;

public class RandomMealGeneratorServlet extends HttpServlet{
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		URL url = new URL("https://www.themealdb.com/api/json/v1/1/random.php");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setRequestMethod("GET");
		connection.connect();
		
		InputStream stream = connection.getInputStream();
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonResponse jsonResponse = objectMapper.readValue(stream, JsonResponse.class);
		
		request.setAttribute("meals", jsonResponse.getMeals().get(0));
		
		RequestDispatcher rd = request.getRequestDispatcher("RandomMealGeneratorResult.jsp");
		rd.forward(request, response);
	}

}