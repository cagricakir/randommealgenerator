package model;

import java.util.List;

public class JsonResponse {
	private List<Meals> meals;

	public List<Meals> getMeals() {
		return meals;
	}

	public void setMeals(List<Meals> meals) {
		this.meals = meals;
	}

}
