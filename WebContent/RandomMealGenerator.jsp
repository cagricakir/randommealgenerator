<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Random Meal Generator</title>
</head>
<body>
	<form action="RandomMealGeneratorServlet" method="post">
		<h1 style="text-align: center;">Feeling Hungry?</h1>
		<h2 style="text-align: center;">Get a random meal by clicking below</h2>
		<div style="text-align: center;"><button style="">Get Meal!</button></div>
	</form>

</body>
</html>