<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="model.Meals" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="RandomMealGenerator.jsp"></jsp:include>

<%Meals meal = (Meals) request.getAttribute("meals"); %>
<table>
	<tr>
		<td><img width="250px" height="250px" src=<%=meal.getStrMealThumb()%>></td>
		<td><h1><%=meal.getStrMeal()%></h1></td>
	</tr>
</table>

</body>
</html>